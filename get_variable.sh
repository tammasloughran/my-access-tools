#!/bin/bash
#PBS -N get_variable
#PBS -P p66
#PBS -q copyq
#PBS -l ncpus=1
#PBS -l mem=50gb
#PBS -l walltime=10:00:00
#PBS -l storage=gdata/p66+gdata/p73
#PBS -l wd
#PBS -M tammas.loughran@csiro.au
# Use NCO to get variables from the archive directory.
# The ACCESS-ESM1-5 archive data are in monthly files with all varables in the same files.
# This is inconvenient so this script fetches some specific variables and concatenates them.

#set -x

WORKING_DIR=/g/data/p66/tfl561/archive_data
#DATA_DIR=/g/data/p73/archive/CMIP6/ACCESS-ESM1-5
DATA_DIR=/g/data/p66/tfl561/ACCESS-ESM

experiment=esm-esm-piNoCrops
variables=(fld_s03i852 fld_s03i853 fld_s03i854 fld_s03i855 fld_s03i856 fld_s03i857 fld_s03i858 fld_s03i859 fld_s03i860 fld_s03i236)
output_names=(cLeaf cWood cRoot cMetabolic cStructural cCoarseWoodyDebris cMicrobial cSlow cPassive tas)
frequency=mon #dai or mon

# Find the ensembles
cd $DATA_DIR
ensembles=($(ls --directory $experiment)) #($(ls --directory $experiment-??))
cd $WORKING_DIR

j=0
for var in ${variables[@]}; do
    echo Getting variable $var
    for e in ${ensembles[@]}; do
        #if [ "${var}" == "fld_s03i851" ]; then
        #    if [ "${e:12:2}" -lt "7" ]; then continue; fi # Continue from an ensemble member.
        #fi
        echo Getting ensemble $e
        # Find files.
        cd ${DATA_DIR}/${e}/history/atm/netCDF
        #pafiles=($(ls *0{301..350}??_${frequency}.nc)) # Only the years in some interval.
        pafiles=($(ls *_${frequency}.nc))
        cd $WORKING_DIR

        # Extract a variable.
        mkdir --parents ${output_names[$j]}
        i=0
        for file in ${pafiles[@]}; do
            ncks -O --variable ${var} ${DATA_DIR}/${e}/history/atm/netCDF/${file} \
                    ${output_names[$j]}/temp.nc
            is_time1=$(ncdump -h ${output_names[$j]}/temp.nc | grep time1)
            if [ $is_time1 ]; then
                timevar=time1
            else
                timevar=time_0
            fi
            ncks -O --mk_rec_dmn $timevar ${output_names[$j]}/temp.nc \
                    ${output_names[$j]}/temp_${e}_$(printf "%04d" ${i}).nc
            i=$(($i + 1))
        done
        rm ${output_names[$j]}/temp.nc

        # Concatenate
        ncrcat ${output_names[$j]}/temp_${e}_????.nc ${output_names[$j]}_${e}.nc
        rm ${output_names[$j]}/temp_${e}_????.nc
        ncrename --variable ${var},${output_names[$j]} ${output_names[$j]}_${e}.nc
    done
    j=$(($j + 1))
done

# Clean up.
rmdir $output_names
